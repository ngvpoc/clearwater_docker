#!/bin/bash
#./scrpt.sh  <newImageName> <oldImageName>
# -newImageName: which is to be saved as copy of existing image with New Name
# -oldImageName: In this image we have to modify and then we will again need to push with the same existing name
#Before Runing the script Please put image id

sudo docker login -u ngvsetup -p ngvsetup@2019
echo "Pulling OldImageName and Pushing with NewImageName"
sudo docker pull ngvsetup/$2:latest
sudo docker images >file1
var1=$(awk '/ngvsetup\/$2/ {print $3}' file1)
sudo docker tag $var1 ngvsetup/$1:latest
sudo docker push ngvsetup/$1:latest

echo "Making New Image with OldImageName"
sudo docker build -f Dockerfile -t $2:latest .

echo "Pushing New Made Image with the Old Image Name"
sudo docker images >file2
var2=$(awk '/ngvsetup\/$1/ {print $3}' file2)
sudo docker tag $var2 ngvsetup/$2:latest
sudo docker push ngvsetup/$2:latest
sudo docker rmi $2 ngvsetup/$1 ngvsetup/$2
